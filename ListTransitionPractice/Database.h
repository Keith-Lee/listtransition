//
//  Database.h
//  ListTransition
//
//  Created by Lavita on 2014/7/21.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Database : NSObject

+ (NSDictionary *)dataDictionary;
- (NSDictionary *)dataDictionary;

@end
