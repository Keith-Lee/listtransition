//
//  NameListTableViewController.h
//  ListTransitionPractice
//
//  Created by Lavita on 2014/7/21.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"

@interface NameListTableViewController : UITableViewController
@property (strong,nonatomic) NSDictionary *databaseDictionary;

@end
