//
//  ImageViewController.h
//  ListTransitionPractice
//
//  Created by Lavita on 2014/7/21.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong,nonatomic) UIImage *picture;
@property (strong,nonatomic) NSString *firstname;

@end
