//
//  Database.m
//  ListTransition
//
//  Created by Lavita on 2014/7/21.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import "Database.h"

@implementation Database

+ (NSDictionary *)dataDictionary{
    return @{
             @"Evan" : @"EvanPic",
             @"Fang" : @"FangPic",
             @"Janux" : @"JanuxPic",
             @"John" : @"JohnPic",
             @"Jonathan" : @"JonathanPic",
             @"Laurence" : @"LaurencePic",
             @"Tim" : @"TimPic",
             @"Wen" : @"WenPic",
             @"Will" : @"WillPic",
             @"William" : @"WilliamPic"
             };
}

- (NSDictionary *)dataDictionary{
    return @{
             @"Evan" : @"EvanPic",
             @"Fang" : @"FangPic",
             @"Janux" : @"JanuxPic",
             @"John" : @"JohnPic",
             @"Jonathan" : @"JonathanPic",
             @"Laurence" : @"LaurencePic",
             @"Tim" : @"TimPic",
             @"Wen" : @"WenPic",
             @"Will" : @"WillPic",
             @"William" : @"WilliamPic"
             };
}

@end
